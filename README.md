```

################### Task 1 :  ####################################################
  Now, Input X is 3D tensor :  (Nsample, mDim, Time)
     Nsample = 10000
     mDim = 3 (3 assets)
     Time = 30
     This is 3D correlated time series : (y1_t, y2_t, y3_t)
      

  TODO :  Change X to 4D tensors :
              Xnew =     (Nsample, mDim,  iNew, Time)

      Xnew[0, :, 0, : ]  = (y1_t, y2_t, y3_t)
      Xnew[0, :, 1, : ]  = (y1_t **2 , y2_t **2 , y3_t **2)
      Xnew[0, :, 2, : ]  = (y1_t **3 , y2_t **3 , y3_t **3)

   
   BUT, LSTM Cannot handle 4D tensor, we use a TRICK :
       Xnew =     (Nsample, mDim*iNew,  Time)      


      Xnew[0, :mDim, : ]               = (y1_t, y2_t, y3_t)
      Xnew[0, mDim:(2*mDim), : ]       = (y1_t **2 , y2_t **2 , y3_t **2)
      Xnew[0, :, 2*mDim:(3*mDim), : ]  = (y1_t **3 , y2_t **3 , y3_t **3)



TFLOW part is implemented here :  
         solver.py/def generate_feed(self)
  

But, 4D data generation is NOT implemented :

https://bitbucket.org/neuralnet01/control/src/master/equation.py#lines-293














################## Files     #########################################################
main.py : File to run the training.

config.py :  configuation file, and NN hyper parameters

solver.py :
   file where the TF graph, train are defined
   globalModel :
       build2 :  Create the model Graph using submodels.py
       train2 :  train our model using generated data self.Sample

       build, train : Not used.

   
submodels.py : where LSTM, FFoward and Attn LSTM are defined , related to solver.py


equation.py 
    where the time series generation, Neural network definition.



utils.py :  Where the time series generator is written.
    mostly a geoemetric brownina motion :
        Xt =X0 . exp(   ut. t +  volt . Wt  )
        dWt being a gaussian process.




################## How it works.  ##################################################
Sample generator   bdse.sample generates time series sample of X :  4D tensor stacked in 3D format.
   X[ nsample,
      ndim_x   :   3 for 3 assets,
      0...T : time steps
     ]
     
   We feed X into a sub-network.
   For each time step, sub-network predicts some value Z ( a tensor of dimension  (nsample, nim_x).
       
   We use z to calculate other values :  w (weights ) and p ( linear weight * X values).    
       
   Final Loss is    the total variance over time of P  + Regualization.
   

2) We export all the paths, weights, into numpy files, 
   we can analyze convergence in Jupyter, ...
  









################### Install
1) install mini conda

2)
conda create -n py36 python=3.6.7

conda install tensorflow=1.14 -c anaconda

conda install -c anaconda pandas scipy scikit-learn seaborn matplotlib 



For fully connected layers:
python  main.py  --do train --problem_name PricingOption  --usemodel ff


For global lstm layer:
python  main.py --do train --problem_name PricingOption  --usemodel lstm

For global lstm with attention:
python  main.py  --do train --problem_name PricingOption  --usemodel attn







### TF Wheels
https://github.com/davidenunes/tensorflow-wheels/releases/download/r1.14.cp37.gpu.xla/tensorflow-1.14.0-cp37-cp37m-linux_x86_64.whl





     
################## Things for improvement :  #######################################################
0) Enhance the state vector X :
   Add 2nd moments.

1) Implement Sequence to Sequence LSTM and test it


2) Regularization  with Graph in TF 2.0


3) Change the Loss








 """ 
                   Filter path with low variance, 
                   Generate all the paths and Maximum difference
                   Monte Carlo Stratified Sampling

 """






















#### AVX optimized       ###########################################
Wheel link: https://github.com/evdcush/TensorFlow-wheels/releases/download/tf-1.13.1-py37-cpu-ivybridge/tensorflow-1.13.1-cp37-cp37m-linux_x86_64.whl

Install via:
pip install --no-cache-dir https://github.com/evdcush/TensorFlow-wheels/releases/download/tf-1.13.1-py37-cpu-ivybridge/tensorflow-1.13.1-cp37-cp37m-linux_x86_64.while


pip install --ignore-installed --upgrade "Download URL" --user



https://github.com/inoryy/tensorflow-optimized-wheels


gitpod /workspace/control $ which gcc
/usr/bin/gcc
gitpod /workspace/control $ gcc --version
gcc (Ubuntu 8.3.0-6ubuntu1~18.10) 8.3.0



```




```

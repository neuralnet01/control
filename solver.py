import logging
import os
import time
import sys

import numpy as np
import tensorflow as tf


####################################################################################################
tf.enable_eager_execution()

TF_DTYPE = tf.float32
MOMENTUM = 0.99
EPSILON = 1e-6
DELTA_CLIP = 50.0

def log(s):
    logging.info(s)


####################################################################################################
from submodels import subnetwork_bidirectionalattn, subnetwork_dila, subnetwork_ff
from submodels import subnetwork_lstm, subnetwork_lstm_attn

from config import export_folder
from stats import save_history, save_stats

if not os.path.exists(export_folder):
    os.makedirs(export_folder)



###################################################################################################
class globalModel(object):
    # Global model over the full time steps period, control output

    def __init__(self, config, bsde, sess, usemodel):
        self._c        = config
        self._bsde     = bsde  # BSDE Equation
        self._sess     = sess  # TF session
        self._usemodel = usemodel

        # make sure consistent with FBSDE equation
        self._dim        = bsde.dim
        self._T          = bsde.num_time_interval
        self._total_time = bsde.total_time

        self._smooth          = 1e-8
        self._is_training     = tf.placeholder(tf.bool)
        self._extra_train_ops = []
        # self._c.num_iterations = None  # "Nepoch"
        # self._train_ops = None  # Gradient, Vale,


        if usemodel == 'attn':    self.subnetwork = subnetwork_lstm_attn(config)
        if usemodel == 'lstm':    self.subnetwork = subnetwork_lstm(config)
        if usemodel == 'ff':      self.subnetwork = subnetwork_ff(config, self._is_training)
        if usemodel == 'dila':    self.subnetwork = subnetwork_dila(config)
        if usemodel == 'biattn':  self.subnetwork = subnetwork_bidirectionalattn(config)
 

    def validation_train(self, feed_dict_valid, train_history, merged, t0, step, val_writer):
        #Evaluate on validation data
        if step % self._c.logging_frequency != 0:  return 0
        loss, init, summary = self._sess.run([self._loss, self._y_init, merged],
                                              feed_dict=feed_dict_valid)
        dt0 = time.time() - t0 + self._t_build
        train_history.append([step, loss, init, dt0])
        print("step: %5u,    loss: %.4e,   Y0: %.4e,  time %3u"% (step, loss, init, dt0))
        val_writer.add_summary(summary, step)


    def generate_feed(self):
        """
        dwl, xl = [], []
        for clayer in range(self._c.clayer):   ## loop on 2nd dimension
            ##### New Sample dynamic at every time
            dw, x = self._bsde.sample(self._c.batch_size, clayer)  #new at each time

            ##### Sample from file , wiht Global counter ii
            # dw, x = self._bsde.sample_fromfile(self._c.batch_size, clayer)
            dwl.append(dw)
            xl.append(x)
            # print("dw.shape", dw.shape,  "x.shape", x.shape )


        ######  4D tensor :  list of 3D tensors   ###############################################
        # dwl, xl = np.stack(dwl, axis=2), np.stack(xl, axis=2)
        """


        dwl, xl = self._bsde.sample(self._c.batch_size, self._c.clayer)  

        # print("new",  [self._c.batch_size, self._c.clayer * self._dim, self._T])
 
        ###### 4D restacked into 3D tensors  ####################################################
        dwl = np.reshape(dwl, [self._c.batch_size, self._c.clayer * self._dim, self._T])
        xl  = np.reshape(xl,  [self._c.batch_size, self._c.clayer * self._dim, self._T + 1])

        return dwl, xl


    def train2(self):
        t0 = time.time()

        ## Validation DATA : Brownian part, drift part from MC simulation  #####################
        dw_valid, x_valid = self.generate_feed()
        feed_dict_valid   = {self._dw: dw_valid, self._x: x_valid, self._is_training: False}

        # update_ops = tf.compat.v1.get_collection(tf.GraphKeys.UPDATE_OPS)  # V1 compatibility
        update_ops      = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        self._train_ops = tf.group([self._train_ops, update_ops])

        # initialization
        self._sess.run(tf.global_variables_initializer())
        val_writer    = tf.summary.FileWriter('logs', self._sess.graph)
        merged        = tf.summary.merge_all()
        train_history = []  # to save iteration results
        x_all, z_all, p_all, w_all, y_all = [], [], [], [], []

        for step in range(self._c.num_iterations + 1):
            # Generate MC sample AS the training input
            dw_train, x_train = self.generate_feed()

            y, p, z, w        = self._sess.run([self._train_ops, self.all_p, self.all_z, self.all_w],
                                        feed_dict={self._dw: dw_train, self._x: x_train, self._is_training: True}, )

            x_train_orig      = np.reshape(x_train, [self._c.batch_size, self._c.dim, self._c.clayer, self._T + 1])
            x_all.append(x_train_orig)
            p_all.append(p); z_all.append(z)
            w_all.append(w); y_all.append(y)

            ### Validation Data Eval.
            self.validation_train(feed_dict_valid, train_history, merged, t0, step, val_writer)

        save_history(export_folder, train_history, x_all, z_all, p_all, w_all, y_all)
        return np.array(train_history)


    def build2(self):
        """
           y : State
           dw : Brownian,  x : drift deterministic
           z : variance
        """
        t0 = time.time()
        TT = np.arange(0, self._bsde.num_time_interval) * self._bsde.delta_t
        M  = self._c.dim  # Nb of assets

        ### Regularization on Loss    ##############################################################
        l1_regularizer = tf.contrib.layers.l1_regularizer(scale=0.01, scope=None)

        #####  EMA regularizer  ##########################
        print("Using EMA ")
        def get_getter(ema): 
            def ema_getter(getter, name, *args, **kwargs):
                var     = getter(name, *args, **kwargs)
                ema_var = ema.average(var)
                return ema_var if ema_var else var
            return ema_getter
        ema = tf.train.ExponentialMovingAverage(decay=0.9999)


        ### dim X Ntime_interval for Stochastic Process  ###########################################
        self._dw = tf.placeholder(TF_DTYPE, [None, self._dim * self._c.clayer, self._T],     name="dW")
        self._x  = tf.placeholder(TF_DTYPE, [None, self._dim * self._c.clayer, self._T + 1], name="X")



        ### Initialization   #######################################################################
 
        #### Dim (abtch_size, Masset)
        # tf.random_uniform([1, self._dim], minval=-0.1, maxval=0.1, dtype=TF_DTYPE) )
        w0        = tf.Variable(tf.random.uniform([self._c.batch_size, M * self._c.clayer], minval=0.1, maxval=0.3, dtype=TF_DTYPE), name='w_init')
        z_init    = tf.Variable( tf.random_uniform([1, self._dim], minval=0.01, maxval=0.3, dtype=TF_DTYPE), name='z_init' )
        all_ones  = tf.ones(shape=tf.stack([tf.shape(self._dw)[0], 1]), dtype=TF_DTYPE, name='all_ones')
        z0        = tf.matmul(all_ones, z_init)


        ##### Dim (batch_size)
        self._y_init = tf.Variable(tf.random_uniform(  [1], minval=self._c.y_init_range[0], maxval=self._c.y_init_range[1], dtype=TF_DTYPE), name='y_init')
        p0           = tf.Variable(tf.random_uniform(shape=[self._c.batch_size], minval=0.0, maxval=0.0, dtype=TF_DTYPE), name='p_init') 


        ##### n_class X label  ####################################################################
        n_class = self._c.dim
        # class_label = tf.random.uniform(shape=[n_class, n_class], name='

        class_label  =  tf.convert_to_tensor( np.array([ [ 0.01, 0.25, 0.74 ],
                                                        [ 0.80, 0.19, 0.01 ],
                                                        [ 0.01, 0.74, 0.25 ],]) , name='class_label', dtype=TF_DTYPE )
        all_p, all_z, all_w, all_y = [p0], [z0], [w0], []
        
        with tf.variable_scope( "forward", custom_getter= get_getter(ema)): ##EMA getter

            ### Time series 0..T
            for t in range(1, self._T):
                # y = (y_old
                #        - self._bsde.delta_t * (self._bsde.f_tf(TT[t], self._x[:, :, t], y_old, z))
                #        + tf.reduce_sum(z * self._dw[:, :, t], 1, keepdims=True))

                #### z is raw value of control #####################################################
                ### We stack multi-dimension to have X as 3D tensor.
                ## Z = [batch, dim * clayer] , dim : nb of assets
                u = self._usemodel 
                if   u == 'lstm':   z = self.subnetwork.build([self._x[:, :, t - 1]], t - 1)          / self._dim
                elif u == 'ff':     z = self.subnetwork.build( self._x[:, :self._dim, t - 1],  t - 1) / self._dim
                elif u == 'attn':   z = self.subnetwork.build([self._x[:, :self._dim, t - 1]], t - 1) / self._dim
                elif u == 'dila':   z = self.subnetwork.build([self._x[:, :self._dim, t - 1]], t - 1) / self._dim
                elif u == 'biattn': z = self.subnetwork.build( self._x[:, :self._dim, t - 1],  t - 1) / self._dim


                #####y intermediate ################################################################
                y = z
                all_y.append(y)


                ### Weight dynamics from z, y, #####################################################
                if t < 3:
                    w = 0.0 + z

                else:
                #    a = 1
                  w = 0.0 + z + 0.005 / tf.sqrt(tf.nn.moments(self._x[:, :M, t-3:t ], axes=2 )[1] +0.001 )
                  w = w / tf.reduce_sum(w , -1, keepdims=True)


                ### t=1 has issue
                # w = 0.0 + z + 1 / tf.sqrt((tf.nn.moments(
                #  tf.log((self._x[:, :M, 1:t ]) / (
                #          self._x[:, :M, 0:t-1 ])), axes=2)[1] + self._smooth))
                # w = z
                # w = w / tf.reduce_sum(w, -1, keepdims=True)  ### Normalize Sum to 1
                # all_w.append(w)

                # Define weights for the log reg, n_classes = dim. z is dim dimensional, use log reg to
                # convert to n_class dimensional.

                # w = z / tf.reduce_sum(z, -1, keepdims=True)
                # w = tf.nn.softmax( z , axis=-1)


                ### Policy-Gradient type : pre-selection of weights    #############################
                ##### From softmax, pick up the right class_label and add dimension 0
                #temperature = 0.04
                #z = tf.nn.softmax( z / temperature, axis=-1)  #Simulate Argmax if temp --> 0

                ## Select right lavel
                #w = tf.linalg.diag(z)
                #w = tf.reduce_sum(tf.matmul(tf.expand_dims(class_label, axis=0), w), axis=1)
                #w = z / tf.reduce_sum(w, -1, keepdims=True)
                """
                W = tf.Variable(tf.zeros([self._dim, n_class]), name='logregw')
                b = tf.Variable(tf.zeros([n_class]), name='logregb')

                # let Y = Wx + b with softmax over classes
                w = tf.nn.softmax(tf.matmul(z, W) + b)
                """

                #### Smoothing in time axis for w ##################################################
                alpha = 0.9   # lower mean high continuity
                if t > 1 :
                  w =  w*alpha + (1-alpha) * all_w[ -1 ]

                ### Cannot get average of weight
                # w  = tf.math.reduce_mean( w, axis=0,keepdims=True, )
                all_w.append(w)
                all_z.append(z)


                #### Portfolio  return #############################################################
                # p =  p_old * (1 + tf.reduce_sum( w * (self._x[:, :, t] / self._x[:, :, t-1] - 1), 1))
                # p = tf.reduce_sum(w * (self._x[:, :M, t] / self._x[:, :M, t - 1] - 1), 1)
                p = tf.reduce_sum(w * (self._x[:, :M, t]), 1)

                all_p.append(p)

            # Terminal time
            # y = (y  - self._bsde.delta_t * self._bsde.f_tf(TT[-1], self._x[:, :, -2], y, z)
            #        + tf.reduce_sum(z * self._dw[:, :, -1], 1, keepdims=True) )
            # y = self._x[:, :, -2]
            # all_y.append(y)

            self.all_y = tf.stack(all_y, axis=-1)
            self.all_z = tf.stack(all_z, axis=-1)
            self.all_w = tf.stack(all_w, axis=-1)
            self.all_p = tf.stack(all_p, axis=-1)
            p = self.all_p



            ##### Final Loss :  ####################################################################
            """ 
               tf.compat.v1.nn.moments(x,  axes, shift=None, name=None, keep_dims=None, keepdims=None )
                  The mean and variance are calculated by aggregating the contents of x across axes. If x is 1-D and axes = [0] this is just the mean and variance of a vector.
               Moment in Sample or Moment in Time ???


                Total_cost = Sum(  -E[ Ri(t) ]  + Var(Rit **2)     )
                reward =  -Ri(t)  + Ri(t)**2

            """
            #######  New Losses
            cost_t = tf.reduce_sum( -p[:,1:]  + tf.square( p[:,1:]  )*10.0 , axis=1)   #sum(0..T)
            self._loss = tf.reduce_mean(cost_t)*10.0


            ####### Old losss
            # #delta =  -tf.reduce_sum(p[:, 1:], 1) + tf.nn.moments(p[:, 1:], axes=1)[1]*10.0
            # delta =  tf.nn.moments(p[:, 1:], axes=1)[1]*10.0
            # self._loss = tf.reduce_mean(delta)*10.0

            ####### Decomposition by time period
            #delta = tf.nn.moments(p[:, 1:10], axes=1)[1] *  00.0 + tf.nn.moments(p[:, 10:20], axes=1)[1] * 10000.0 + \
            #        tf.nn.moments(p[:, 20:], axes=1)[1] * 10000.0  # \
            #        #+ 5.0 * tf.abs(1- tf.reduce_sum(all_w[-1], axis=-1)[0])


            #### Moving Average in Sample
            #  delta = moving_average( delta,  all_delta)


            ####  Regularization
            weights     = tf.trainable_variables()  # all vars of your graph
            reg_penalty = tf.contrib.layers.apply_regularization(l1_regularizer, weights)

            self._loss = self._loss + reg_penalty  # this loss needs to be minimized
            ########################################################################################

        tf.summary.scalar('loss', self._loss)


        ########## train operations
        global_step = tf.get_variable( "global_step", [], initializer=tf.constant_initializer(0), trainable=False, dtype=tf.int32, )
        lr_rate     = tf.train.piecewise_constant(global_step, self._c.lr_boundaries, self._c.lr_values)

        train_vars  = tf.trainable_variables()
        grads       = tf.gradients(self._loss, train_vars)
        optimizer   = tf.train.AdamOptimizer(learning_rate=lr_rate)
        apply_op    = optimizer.apply_gradients(zip(grads, train_vars), global_step=global_step, name="train_step" )
        all_ops     = [apply_op] + self._extra_train_ops


        # Training with EMA averages  #############################################################
        with tf.control_dependencies([ apply_op ]):
           all_ops_ema = ema.apply( train_vars )

        
        self._train_ops = tf.group(*[all_ops_ema] )
        self._t_build   = time.time() - t0



    def sample_files(self, X, dW, idx):
        bs    = self._c.batch_size
        dw, x = dW[bs * idx:bs * (idx + 1), ...], X[bs * idx:bs * (idx + 1), ...]

        dw_valid = np.reshape(dw, [self._c.batch_size, self._c.clayer * self._dim, self._T])
        x_valid  = np.reshape(x, [self._c.batch_size, self._c.clayer * self._dim, self._T + 1])
        return dw_valid, x_valid



    def predict_sequence(self, input_folder, output_folder="out/"):
        # Given an input of time series sample, predict the sequences.
        saver = tf.train.Saver()
        saver.restore(self._sess, input_folder + '/model.ckpt')

        x_all, dw_all = np.load(input_folder + "/x.npz"), np.load(input_folder + "/dw.npz")
        n             = x_all.shape[0]

        p_all, z_all, w_all, x_all, y_all = [], [], [], [], []

        print("Predicting over the sequence, results will be saved as np array...")
        for idx in range(n // self._c.batch_size):
            dw, x   = self.sample_files(x_all, dw_all, idx)
            p, z, w = self._sess.run([self.all_p, self.all_z, self.all_w],
                                      feed_dict={self._dw: dw, self._x: x, self._is_training: False})

            y = []
            p_all.append(p)
            z_all.append(z)
            w_all.append(w)
            x_all.append(x)
            y_all.append(y)

        p_all = np.concatenate(p_all, axis=0)
        z_all = np.concatenate(z_all, axis=0)
        w_all = np.concatenate(w_all, axis=0)
        x_all = np.concatenate(x_all, axis=0)
        y_all = np.concatenate(y_all, axis=0)

        save_history([], x_all, z_all, p_all, w_all, y_all)







    ################################################################################################
    ################################################################################################
    def train(self):
        t0 = time.time()
        ## Validation DATA : Brownian part, drift part from MC simulation
        # dw_valid, x_valid = self._bsde.sample(self._c.batch_size)
        #################################################################
        dw_valid, x_valid = self.generate_feed()
        feed_dict_valid = {self._dw: dw_valid, self._x: x_valid, self._is_training: False}

        # update_ops = tf.compat.v1.get_collection(tf.GraphKeys.UPDATE_OPS)  # V1 compatibility
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
        self._train_ops = tf.group([self._train_ops, update_ops])

        # initialization
        self._sess.run(tf.global_variables_initializer())
        val_writer = tf.summary.FileWriter('logs', self._sess.graph)
        merged = tf.summary.merge_all()
        train_history = []  # to save iteration results

        for step in range(self._c.num_iterations + 1):
            # Generate MC sample AS the training input
            dw_train, x_train = self.generate_feed()

            self._sess.run(self._train_ops,
                           feed_dict={self._dw: dw_train,
                                      self._x: x_train, self._is_training: True}, )

            ### Validation Data Eval.
            self.validation_train(feed_dict_valid, train_history, merged, t0, step, val_writer)

        return np.array(train_history)

    ###################### Previous model build  ####################################################
    def build(self):
        """"
           y : State
           dw : Brownian,  x : drift deterministic
           z : variance
        """
        t0 = time.time()
        TT = np.arange(0, self._bsde.num_time_interval) * self._bsde.delta_t

        ### dim X Ntime_interval for Stochastic Process  ############################################
        self._dw = tf.placeholder(TF_DTYPE,  [None, self._c.clayer * self._dim, self._T], name="dW")
        self._x = tf.placeholder(TF_DTYPE, [None, self._c.clayer * self._dim,   self._T + 1],  name="X")

        ### Initialization
        ## x : state,  Cost
        self._y_init = tf.Variable(
            tf.random_uniform(
                [1], minval=self._c.y_init_range[0], maxval=self._c.y_init_range[1],
                dtype=TF_DTYPE, name='y_iniy'
            )
        )

        ## Control
        z_init = tf.Variable(
            tf.random_uniform([1, self._dim], minval=-0.1, maxval=0.1, dtype=TF_DTYPE), name='z_init')
        all_ones = tf.ones(shape=tf.stack([tf.shape(self._dw)[0], 1]), dtype=TF_DTYPE, name='all_ones')
        y = all_ones * self._y_init
        z = tf.matmul(all_ones, z_init)

        with tf.variable_scope("forward"):
            for t in range(0, self._T - 1):
                y = (
                        y
                        - self._bsde.delta_t * (
                            self._bsde.f_tf(TT[t], self._x[:, :self._dim, t], y, z))
                        + tf.reduce_sum(z * self._dw[:, :self._dim, t], 1, keepdims=True)
                )

                ## Neural Network per Time Step, Calculate Gradient
                if self._usemodel == 'lstm': z = self.subnetwork.build([self._x[:, :, t + 1]], t) / self._dim
                elif self._usemodel == 'ff':   z = self.subnetwork.build(self._x[:, :, t + 1], t) / self._dim
                elif self._usemodel == 'attn':  z = self.subnetwork.build([self._x[:, :, t + 1]], t) / self._dim
                elif self._usemodel == 'dila':  z = self.subnetwork.build([self._x[:, :, t + 1]], t) / self._dim
                elif self._usemodel == 'biattn':   z = self.subnetwork.build(self._x[:, :, t + 1], t) / self._dim

            # Terminal time
            y = (
                    y
                    - self._bsde.delta_t * self._bsde.f_tf(TT[-1], self._x[:, :self._dim, -2], y, z)
                    + tf.reduce_sum(z * self._dw[:, :self._dim, -1], 1, keepdims=True)
            )

            # Final Difference :
            delta = y - self._bsde.g_tf(self._total_time, self._x[:, :self._dim, -1])

            # use linear approximation outside the clipped range
            self._loss = tf.reduce_mean(
                tf.where(
                    tf.abs(delta) < DELTA_CLIP,
                    tf.square(delta),
                    2 * DELTA_CLIP * tf.abs(delta) - DELTA_CLIP ** 2,
                )
            )
        tf.summary.scalar('loss', self._loss)

        # train operations
        global_step = tf.get_variable(
            "global_step", [],
            initializer=tf.constant_initializer(0),
            trainable=False,
            dtype=tf.int32,
        )

        learning_rate = tf.train.piecewise_constant(
            global_step, self._c.lr_boundaries, self._c.lr_values
        )

        trainable_variables = tf.trainable_variables()
        grads = tf.gradients(self._loss, trainable_variables)
        optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)
        apply_op = optimizer.apply_gradients(
            zip(grads, trainable_variables), global_step=global_step, name="train_step"
        )
        all_ops = [apply_op] + self._extra_train_ops
        self._train_ops = tf.group(*all_ops)
        self._t_build = time.time() - t0








